import turtle

tlo = turtle.Screen()
tlo.bgcolor("black")
tur = turtle.Turtle()
tur.shape("turtle")
tur.speed(0)
tur.up()
tur.goto(-150, 200)
tur.down()
tur.pencolor("darkblue")
tur.pensize(2)

a = 300
b = 4


def snowkoch(lenght, order):
    if order == 0:
        tur.forward(lenght)
    else:
        snowkoch(lenght / 3, order - 1)
        tur.right(60)
        snowkoch(lenght / 3, order - 1)
        tur.begin_fill()
        tur.fillcolor("white")
        tur.left(120)
        snowkoch(lenght / 3, order - 1)
        tur.right(60)
        snowkoch(lenght / 3, order - 1)
        tur.end_fill()


for i in range(6):
    snowkoch(a, b)
    tur.right(60)
