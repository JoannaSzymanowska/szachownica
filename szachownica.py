import turtle

tlo = turtle.Screen()
tlo.bgcolor("light blue")  # nadałam kolor tła
tur = turtle.Turtle()  # nazwałam swojego żółwia
tur.shape("turtle")  # nadałam kształt swojemu żółwiowi
tur.speed(0)  # nadałam tempa swojemu żółwiowi
tur.up()
tur.goto(-460, 400)  # przeniosłam zółwia w miejsce startowe
tur.down()
bok = 80  # wymiar boku kwadratu
ilosc = 11  # ilość kwadratów w linii
iloscAkcji = 0

for kwadrat in range(4):
    tur.pensize(10)  # stworzyłam ramkę szachownicy
    tur.forward(880)
    tur.right(90)


def szachownica(bok):
    tur.begin_fill()
    tur.pensize(2)
    for kwadracik in range(4):  # zdefiniowałam kwadracik
        tur.forward(bok)
        tur.right(90)


for iloscKadratowPoziomo in range(11):  # usatliłam ile razy czynność ma sie powtarzać
    for linia in range(ilosc):
        iloscAkcji += 1
        szachownica(bok)
        tur.forward(bok)
        if iloscAkcji % 2:
            tur.fillcolor("black")  # nadałam kolory
            tur.end_fill()
        else:
            tur.fillcolor("white")
            tur.end_fill()
    tur.backward(ilosc * bok)  # kazałam wrócić żółwiowi na początek linii
    tur.right(90)
    tur.forward(bok)  # kazałam żółwiowi przesunąć się linie niżej
    tur.left(90)
